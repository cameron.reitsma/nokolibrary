using System.Text.RegularExpressions;

namespace NokoClient;
//Code Taken from --> https://gist.github.com/pimbrouwers/8f78e318ccfefff18f518a483997be29
public class LinkHeader
{
 public string? First { get; set; }
 public string? Previous { get; set; }
 public string? Next { get; set; }
 public string? Last { get; set; }

 public void GetLinksFromHeader(string linkHeaderStrP)
 {
  string[] link_strings = linkHeaderStrP.Split(',');

  if (link_strings != null && link_strings.Any())
  {
   foreach (var link_string in link_strings)
   {
    var rel_match = Regex.Match(link_string, "(?<=rel=\").+?(?=\")", RegexOptions.IgnoreCase);
    var link_match = Regex.Match(link_string, "(?<=<).+?(?=>)", RegexOptions.IgnoreCase);

    if (rel_match.Success && link_match.Success)
    {
     var rel = rel_match.Value.ToUpper();
     var link = link_match.Value;

     switch (rel)
     {
      case "FIRST":
       First = link;
       break;
      case "PREV":
       Previous = link;
       break;
      case "NEXT":
       Next = link;
       break;
      case "LAST":
       Last = link;
       break;
     }
    }
   }
  }
 }
}
