﻿using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Text.Json;
using System.Text.Json.Serialization;
using Microsoft.AspNetCore.WebUtilities;
using NokoClient.Model;

namespace NokoClient;

public class NokoClientV2 : INokoClient
{
  public NokoClientV2(string accessTokenP, string userAgentP)
  {
    _nokoClient = new HttpClient { BaseAddress = new Uri("https://api.nokotime.com/v2/") };
    _nokoClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
    _nokoClient.DefaultRequestHeaders.UserAgent.Add(new ProductInfoHeaderValue(userAgentP, "1.0"));
    _nokoClient.DefaultRequestHeaders.Add("X-NokoToken", accessTokenP);
    _jsonOptions = new JsonSerializerOptions
    {
      PropertyNameCaseInsensitive = true,
      DefaultIgnoreCondition = JsonIgnoreCondition.WhenWritingNull
    };
  }

  private readonly HttpClient _nokoClient;
  private readonly JsonSerializerOptions _jsonOptions;

  public async Task<List<NokoProject>> GetProjectsAsync(IDictionary<string, string>? queryParamsP = null)
  {
    return await GetAllAsync<NokoProject>("projects", queryParamsP);
  }//Works

  public async Task<NokoProject> GetProjectAsync(int idP)
  {
    return await GetAsync<NokoProject>($"projects/{idP}");
  }//Works

  public async Task<HttpResponseMessage> CreateProjectAsync(NokoProjectCreator nokoProjectP)
  {
    return await PostAsync("projects", nokoProjectP);
  }//Works

  public async Task<List<NokoEntry>> GetProjectEntriesAsync(int projectIdP,
    IDictionary<string, string>? queryParamsP = null)
  {
    return await GetAllAsync<NokoEntry>($"projects/{projectIdP}/entries", queryParamsP);
  }//Works

  public async Task<List<NokoExpense>> GetProjectExpensesAsync(int projectIdP,
    IDictionary<string, string>? queryParamsP = null)
  {
    return await GetAllAsync<NokoExpense>($"projects/{projectIdP}/expenses", queryParamsP);
  }//Works

  public async Task<HttpResponseMessage> EditProjectAsync(int idP, NokoProjectEditor nokoProjectP)
  {
    return await PutAsync($"projects/{idP}", nokoProjectP);
  }//Works

  public async Task<HttpResponseMessage> MergeProjectsAsync(int sourceProjectIdP, int projectToBeMergedIdP)
  {
    var input = new { project_id = projectToBeMergedIdP };
    return await PutAsync($"projects/{sourceProjectIdP}/merge", input);
  }//Works

  public async Task<HttpResponseMessage> DeleteProjectAsync(int projectIdP)
  {
    return await DeleteAsync($"projects/{projectIdP}");
  }//Works

  public async Task<HttpResponseMessage> ArchiveProjectAsync(int projectIdP)
  {
    return await _nokoClient.PutAsync($"projects/{projectIdP}/archive", null);
  }//Works

  public async Task<HttpResponseMessage> UnarchiveProjectAsync(int projectIdP)
  {
    return await _nokoClient.PutAsync($"projects/{projectIdP}/unarchive", null);
  }//Works

  public async Task<HttpResponseMessage> ArchiveMultipleProjectsAsync(int[] projectIdsP)
  {
    var input = new { project_ids = projectIdsP };
    return await PutAsync("projects/archive", input);
  }//Works

  public async Task<HttpResponseMessage> UnarchiveMultipleProjectsAsync(int[] projectIdsP)
  {
    var input = new { project_ids = projectIdsP };
    return await PutAsync("projects/unarchive", input);
  }//Works


  public async Task<HttpResponseMessage> DeleteMultipleProjectsAsync(int[] projectIdsP)
  {
    var input = new { project_ids = projectIdsP };
    return await PutAsync("projects/delete", input);
  }//Works

  public async Task<List<NokoTag>> GetTagsAsync(IDictionary<string, string>? queryParamsP = null)
  {
    return await GetAllAsync<NokoTag>("tags", queryParamsP);
  }//Works

  public async Task<HttpResponseMessage> CreateMultipleTagsAsync(string[] namesP)
  {
    var tag_creator = new NokoTagCreator { Names = namesP };
    return await PostAsync("tags", tag_creator);
  }//Works

  public async Task<NokoTag> GetTagAsync(int tagIdP)
  {
    return await GetAsync<NokoTag>($"tags/{tagIdP}");
  }//Works

  public async Task<List<NokoEntry>> GetTagEntriesAsync(int tagIdP, IDictionary<string, string>? queryParamsP = null)
  {
    return await GetAllAsync<NokoEntry>($"tags/{tagIdP}/entries", queryParamsP);
  }//Works

  public async Task<HttpResponseMessage> EditTagAsync(int idP, string nameP)
  {
    var input = new { name = nameP };
    return await PutAsync($"tags/{idP}", input);
  }//Works

  public async Task<HttpResponseMessage> MergeTagsAsync(int sourceTagIdP, int tagToBeMergedIdP)
  {
    var input = new { tag_id = tagToBeMergedIdP };
    return await PutAsync($"tags/{sourceTagIdP}/merge", input);
  }//Works

  public async Task<HttpResponseMessage> DeleteTagAsync(int idP)
  {
    return await DeleteAsync($"tags/{idP}");
  }//Works

  public async Task<HttpResponseMessage> DeleteMultipleTagsAsync(int[] tagIdsP)
  {
    var input = new { tag_ids = tagIdsP };
    return await PutAsync("tags/delete", input);
  }//Works

  public async Task<List<NokoTimer>> GetTimersAsync(IDictionary<string, string>? queryParamsP = null)
  {
    return await GetAllAsync<NokoTimer>("timers", queryParamsP);
  }//Works

  public async Task<NokoTimer> GetProjectTimerAsync(int projectIdP)
  {
    return await GetAsync<NokoTimer>($"projects/{projectIdP}/timer");
  }//Works

  public async Task<HttpResponseMessage> EditProjectTimerAsync(int projectIdP, string descriptionP)
  {
    var input = new { description = descriptionP };
    return await PutAsync($"projects/{projectIdP}/timer", input);
  }//Works

  public async Task<HttpResponseMessage> StartProjectTimerAsync(int projectIdP, DateOnly? dateP = null, string? descriptionP = null)
  {
    var input = new { date = dateP, description = descriptionP };
    return await PutAsync($"projects/{projectIdP}/timer/start", input);
  }//Works

  public async Task<HttpResponseMessage> PauseProjectTimerAsync(int projectIdP)
  {
    return await _nokoClient.PutAsync($"projects/{projectIdP}/timer/pause", null);
  }//Works

  public async Task<HttpResponseMessage> AddOrSubtractFromTimerAsync(int projectIdP, int? minutesP = null, int? secondsP = null)
  {
    var input = new { minutes = minutesP, seconds = secondsP };
    return await PutAsync($"projects/{projectIdP}/timer/add_or_subtract_time", input);
  }//Works

  public async Task<HttpResponseMessage> LogProjectTimerAsync(int projectIdP, DateOnly? entryDateP = null, int? minutesP = null,
    string? descriptionP = null)
  {
    var input = new { entry_date = entryDateP, minutes = minutesP, description = descriptionP };
    return await PutAsync($"projects/{projectIdP}/timer/log", input);
  }//Works

  public async Task<HttpResponseMessage> DiscardProjectTimerAsync(int projectIdP)
  {
    return await _nokoClient.DeleteAsync($"projects/{projectIdP}/timer");
  }//Works

  public async Task<List<NokoProjectGroup>> GetProjectGroupsAsync(IDictionary<string, string>? queryParamsP = null)
  {
    return await GetAllAsync<NokoProjectGroup>("project_groups", queryParamsP);
  }//Works

  public async Task<NokoProjectGroup> GetProjectGroupAsync(int projectGroupIdP)
  {
    return await GetAsync<NokoProjectGroup>($"project_groups/{projectGroupIdP}");
  }//Works

  public async Task<HttpResponseMessage> CreateProjectGroupAsync(NokoProjectGroupCreator nokoProjectGroupP)
  {
    return await PostAsync("project_groups", nokoProjectGroupP);
  }//Works

  public async Task<HttpResponseMessage> EditProjectGroupAsync(int projectGroupIdP, string nameP)
  {
    var input = new { name = nameP };
    return await PutAsync($"project_groups/{projectGroupIdP}", input);
  }//Works

  public async Task<List<NokoEntry>> GetProjectGroupProjectEntriesAsync(int projectGroupIdP,
    IDictionary<string, string>? queryParamsP = null)
  {
    return await GetAllAsync<NokoEntry>($"project_groups/{projectGroupIdP}/entries", queryParamsP);
  }//Works

  public async Task<List<NokoProject>> GetProjectGroupProjectsAsync(int projectGroupIdP,
    IDictionary<string, string>? queryParamsP = null)
  {
    return await GetAllAsync<NokoProject>($"project_groups/{projectGroupIdP}/projects", queryParamsP);
  }//Works

  public async Task<HttpResponseMessage> AddProjectsToProjectGroupAsync(int projectGroupIdP, int[] projectIdsP)
  {
    var input = new { project_ids = projectIdsP };
    return await PostAsync($"/project_groups/{projectGroupIdP}/add_projects", input);
  }//NOT WORKING

  public async Task<HttpResponseMessage> RemoveProjectsFromProjectGroupAsync(int projectGroupIdP,
    IDictionary<string, int[]> projectsToRemoveP)
  {
    return await PutAsync($"project_groups/{projectGroupIdP}/remove_projects", projectsToRemoveP);
  }

  public async Task<HttpResponseMessage> RemoveAllProjectsFromProjectGroupAsync(int projectGroupIdP)
  {
    return await _nokoClient.PutAsync($"project_groups/{projectGroupIdP}/remove_all_projects", null);
  }//Works

  public async Task<HttpResponseMessage> DeleteProjectGroupAsync(int projectGroupIdP)
  {
    return await DeleteAsync($"project_groups/{projectGroupIdP}");
  }//Works

  public async Task<List<NokoEntry>> GetEntriesAsync(IDictionary<string, string>? queryParamsP = null)
  {
    return await GetAllAsync<NokoEntry>("entries", queryParamsP);
  }//Works

  public async Task<NokoEntry> GetEntryAsync(int entryIdP)
  {
    return await GetAsync<NokoEntry>($"entries/{entryIdP}");
  }//Works

  public async Task<HttpResponseMessage> CreateEntryAsync(NokoEntryCreator nokoEntryP)
  {
    return await PostAsync("entries", nokoEntryP);
  }//Works

  public async Task<HttpResponseMessage> EditEntryAsync(int entryIdP, NokoEntryEditor nokoEntryP)
  {
    return await PutAsync($"entries/{entryIdP}", nokoEntryP);
  }//Works

  public async Task<HttpResponseMessage> MarkEntryAsInvoicedAsync(int entryIdP, DateOnly dateP)
  {
    var input = new { date = dateP };
    return await PutAsync($"entries/{entryIdP}/marked_as_invoiced", input);
  }//Works TODO change to DateTime instead of DateOnly

  public async Task<HttpResponseMessage> MarkMultipleEntriesAsInvoicedAsync(int[] entryIdsP, DateOnly dateP)
  {
    var input = new { entry_ids = entryIdsP, date = dateP };
    return await PutAsync("entries/marked_as_invoiced", input);
  }//TODO Does not seem to be marking asa invoices. But is returning a success code, also change to DateTime same as above

  public async Task<HttpResponseMessage> MarkEntryApprovedAsync(int entryIdP, DateOnly? dateP = null)
  {
    var input = new { date = dateP };
    return await PutAsync($"entries/{entryIdP}/approved", input);
  }//Works

  public async Task<HttpResponseMessage> MarkMultipleEntriesAsApprovedAsync(int[] entryIdsP, DateOnly? approvedAtP = null)
  {
    var input = new { entry_ids = entryIdsP, approved_at = approvedAtP };
    return await PutAsync("entries/approved", input);
  }//Works

  public async Task<HttpResponseMessage> MarkEntryUnapprovedAsync(int entryIdP)
  {
    return await _nokoClient.PutAsync($"entries/{entryIdP}/unapproved", null);
  }//Works

  public async Task<HttpResponseMessage> MarkMultipleEntriesAsUnapprovedAsync(int[] entryIdsP)
  {
    var input = new { entry_ids = entryIdsP };
    return await PutAsync("entries/unapproved", input);
  }//Works

  public async Task<HttpResponseMessage> DeleteEntryAsync(int entryIdP)
  {
    return await DeleteAsync($"entries/{entryIdP}");
  }//Works

  public async Task<List<NokoInboxEntry>> GetInboxEntriesAsync(IDictionary<string, string>? queryParamsP = null)
  {
    return await GetAllAsync<NokoInboxEntry>("inbox_entries", queryParamsP);
  }//Works

  public async Task<NokoInboxEntry> GetInboxEntryAsync(int entryIdP)
  {
    return await GetAsync<NokoInboxEntry>($"inbox_entries/{entryIdP}");
  }//Works

  public async Task<HttpResponseMessage> CreateInboxEntryAsync(NokoInboxEntryCreator nokoInboxEntryP)
  {
    return await PostAsync("inbox_entries", nokoInboxEntryP);
  }//Works

  public async Task<HttpResponseMessage> EditInboxEntryAsync(int entryIdP, NokoInboxEntryEditor nokoInboxEntryP)
  {
    return await PutAsync($"inbox_entries/{entryIdP}", nokoInboxEntryP);
  }//Works

  public async Task<HttpResponseMessage> LogInboxEntryAsync(int entryIdP, NokoEntryCreator nokoEntryP)
  {
    return await PostAsync($"inbox_entries/{entryIdP}/log", nokoEntryP);
  }//Works

  public async Task<HttpResponseMessage> DeleteInboxEntryAsync(int entryIdP)
  {
    return await DeleteAsync($"inbox_entries/{entryIdP}");
  }//Works

  public async Task<List<NokoInvoice>> GetInvoicesAsync(IDictionary<string, string>? queryParamsP = null)
  {
    return await GetAllAsync<NokoInvoice>("invoices", queryParamsP);
  }//Works

  public async Task<NokoInvoice> GetInvoiceAsync(int invoiceIdP)
  {
    return await GetAsync<NokoInvoice>($"invoices/{invoiceIdP}");
  }

  public async Task<HttpResponseMessage> CreateInvoiceAsync(NokoInvoiceCreator nokoInvoiceP)
  {
    return await PostAsync("invoices", nokoInvoiceP);
  }//Works

  public async Task<HttpResponseMessage> EditInvoiceAsync(int invoiceIdP, NokoInvoiceEditor nokoInvoiceP)
  {
    return await PutAsync($"invoices/{invoiceIdP}", nokoInvoiceP);
  }

  public async Task<HttpResponseMessage> MarkInvoicePayedAsync(int invoiceIdP)
  {
    return await _nokoClient.PutAsync($"invoices/{invoiceIdP}/paid", null);
  }

  public async Task<HttpResponseMessage> MarkInvoiceUnpaidAsync(int invoiceIdP)
  {
    return await _nokoClient.PutAsync($"invoices/{invoiceIdP}/unpaid", null);
  }

  public async Task<List<NokoEntry>> GetInvoiceEntriesAsync(int invoiceIdP,
    IDictionary<string, string>? queryParamsP = null)
  {
    return await GetAllAsync<NokoEntry>($"invoices/{invoiceIdP}/entries", queryParamsP);
  }

  public async Task<List<NokoExpense>> GetInvoiceExpensesAsync(int invoiceIdP,
    IDictionary<string, string>? queryParamsP = null)
  {
    return await GetAllAsync<NokoExpense>($"invoices/{invoiceIdP}/expenses", queryParamsP);
  }

  public async Task<HttpResponseMessage> AddEntriesToInvoiceAsync(int invoiceIdP, int[] entryIdsP)
  {
    var input = new { entry_ids = entryIdsP };
    return await PutAsync($"invoices/{invoiceIdP}/remove_entries", input);
  }

  public async Task<HttpResponseMessage> RemoveEntriesFromInvoiceAsync(int invoiceIdP, int[] entryIdsP)
  {
    var input = new { entry_ids = entryIdsP };
    return await PutAsync($"invoices/{invoiceIdP}/remove_entries", input);
  }

  public async Task<HttpResponseMessage> RemoveAllEntriesFromInvoiceAsync(int invoiceIdP)
  {
    return await _nokoClient.PutAsync($"invoices/{invoiceIdP}/remove_all_entries", null);
  }

  public async Task<HttpResponseMessage> AddExpensesToInvoiceAsync(int invoiceIdP, int[] expenseIdsP)
  {
    var input = new { expense_ids = expenseIdsP };
    return await PutAsync($"/invoices/{invoiceIdP}/add_expenses", input);
  }

  public async Task<HttpResponseMessage> RemoveExpensesFromInvoiceAsync(int invoiceIdP, int[] expenseIdsP)
  {
    var input = new { expense_ids = expenseIdsP };
    return await PutAsync($"invoices/{invoiceIdP}/remove_expenses", input);
  }

  public async Task<HttpResponseMessage> RemoveAllExpensesFromInvoiceAsync(int invoiceIdP)
  {
    return await _nokoClient.PutAsync($"invoices/{invoiceIdP}/remove_all_expenses", null);
  }

  public async Task<HttpResponseMessage> AddTaxesToInvoiceAsync(int invoiceIdP, NokoTaxCreator[] nokoTaxCreatorsP)
  {
    return await PutAsync($"invoices/{invoiceIdP}/add_taxes", nokoTaxCreatorsP);
  }

  public async Task<HttpResponseMessage> RemoveTaxesFromInvoiceAsync(int invoiceIdP, int[] taxIdsP)
  {
    var input = new { tax_ids = taxIdsP };
    return await PutAsync($"invoices/{invoiceIdP}/remove_taxes", input);
  }

  public async Task<HttpResponseMessage> RemoveAllTaxesFromInvoiceAsync(int invoiceIdP)
  {
    return await _nokoClient.PutAsync($"invoices/{invoiceIdP}/remove_all_taxes", null);
  }

  public async Task<HttpResponseMessage> DeleteInvoiceAsync(int invoiceIdP)
  {
    return await DeleteAsync($"invoices/{invoiceIdP}");
  }

  public async Task<List<NokoExpense>> GetExpensesAsync(IDictionary<string, string>? queryParamsP = null)
  {
    return await GetAllAsync<NokoExpense>($"expenses", queryParamsP);
  }

  public async Task<NokoExpense> GetExpenseAsync(int expenseIdP)
  {
    return await GetAsync<NokoExpense>($"expenses/{expenseIdP}");
  }

  public async Task<HttpResponseMessage> CreateExpenseAsync(NokoExpenseCreator nokoExpenseP)
  {
    return await PostAsync("expenses", nokoExpenseP);
  }

  public async Task<HttpResponseMessage> EditExpenseAsync(int expenseIdP, NokoExpenseEditor nokoExpenseP)
  {
    return await PutAsync($"expenses/{expenseIdP}", nokoExpenseP);
  }

  public async Task<HttpResponseMessage> DeleteExpenseAsync(int expenseIdP)
  {
    return await DeleteAsync($"expenses/{expenseIdP}");
  }

  public async Task<NokoAccount> GetAccountAsync()
  {
    return await GetAsync<NokoAccount>("account");
  }

  public async Task<NokoCurrentUser> GetCurrentUserAsync()
  {
    return await GetAsync<NokoCurrentUser>("current_user");
  }

  public async Task<List<NokoEntry>> GetCurrentUserEntriesAsync(IDictionary<string, string>? queryParamsP = null)
  {
    return await GetAllAsync<NokoEntry>("current_user/entries");
  }

  public async Task<List<NokoExpense>> GetCurrentUserExpensesAsync(IDictionary<string, string>? queryParamsP = null)
  {
    return await GetAllAsync<NokoExpense>("current_user/expenses");
  }

  public async Task<HttpResponseMessage> EditCurrentUserAsync(NokoCurrentUserEditor currentUserP)
  {
    return await PutAsync("current_user", currentUserP);
  }

  public async Task<List<NokoUser>> GetUsersAsync(IDictionary<string, string>? queryParamsP = null)
  {
    return await GetAllAsync<NokoUser>("users", queryParamsP);
  }

  public async Task<NokoUser> GetUserAsync(int userIdP)
  {
    return await GetAsync<NokoUser>($"users/{userIdP}");
  }

  public async Task<List<NokoEntry>> GetUserEntriesAsync(int userIdP, IDictionary<string, string>? queryParamsP = null)
  {
    return await GetAllAsync<NokoEntry>($"users/{userIdP}/entries", queryParamsP);
  }

  public async Task<List<NokoExpense>> GetUserExpensesAsync(int userIdP,
    IDictionary<string, string>? queryParamsP = null)
  {
    return await GetAllAsync<NokoExpense>($"users/{userIdP}/expenses", queryParamsP);
  }

  public async Task<HttpResponseMessage> CreateUserAsync(NokoUserCreator nokoUserP)
  {
    return await PostAsync("users", nokoUserP);
  }

  public async Task<HttpResponseMessage> EditUserAsync(int userIdP, NokoUserEditor nokoUserP)
  {
    return await PutAsync($"users/{userIdP}", nokoUserP);
  }

  public async Task<HttpResponseMessage> ReactivateUserAsync(int userIdP)
  {
    return await _nokoClient.PutAsync($"users/{userIdP}/activate", null);
  }

  public async Task<HttpResponseMessage> GiveUserAccessToProjectsAsync(int userIdP, int[] projectIdsP)
  {
    var input = new { project_ids = projectIdsP };
    return await PutAsync($"users/{userIdP}/give_access_to_projects", input);
  }

  public async Task<HttpResponseMessage> RevokeUserAccessToProjectsAsync(int userIdP, int[] projectIdsP)
  {
    var input = new { project_ids = projectIdsP };
    return await PutAsync($"users/{userIdP}/revoke_access_to_projects", input);
  }

  public async Task<HttpResponseMessage> RevokeUserAccessToAllProjectsAsync(int userIdP)
  {
    return await _nokoClient.PutAsync($"users/{userIdP}/revoke_access_to_all_projects", null);
  }

  public async Task<HttpResponseMessage> DeleteUserAsync(int userIdP)
  {
    return await DeleteAsync($"users/{userIdP}");
  }

  public async Task<HttpResponseMessage> DeactivateUserAsync(int userIdP)
  {
    return await _nokoClient.PutAsync($"users/{userIdP}/deactivate", null);
  }

  public async Task<List<NokoTeam>> GetTeamsAsync(IDictionary<string, string>? queryParamsP = null)
  {
    return await GetAllAsync<NokoTeam>($"teams", queryParamsP);
  }

  public async Task<NokoTeam> GetTeamAsync(int teamIdP)
  {
    return await GetAsync<NokoTeam>($"teams/{teamIdP}");
  }

  public async Task<HttpResponseMessage> CreateTeamAsync(NokoTeamCreator nokoTeamP)
  {
    return await PostAsync("teams", nokoTeamP);
  }

  public async Task<HttpResponseMessage> EditTeamAsync(int teamIdP, string teamNameP)
  {
    var input = new { name = teamNameP };
    return await PutAsync($"teams/{teamIdP}", input);
  }

  public async Task<List<NokoEntry>> GetTeamUserEntriesAsync(int teamIdP,
    IDictionary<string, string>? queryParamsP = null)
  {
    return await GetAllAsync<NokoEntry>($"teams/{teamIdP}/entries", queryParamsP);
  }

  public async Task<List<NokoUser>> GetTeamUsersAsync(int teamIdP, IDictionary<string, string>? queryParamsP = null)
  {
    return await GetAllAsync<NokoUser>($"teams/{teamIdP}/users", queryParamsP);
  }

  public async Task<HttpResponseMessage> AddUsersToTeamAsync(int teamIdP, int[] userIdsP)
  {
    var input = new { user_ids = userIdsP };
    return await PostAsync($"teams/{teamIdP}/add_users", input);
  }

  public async Task<HttpResponseMessage> RemoveUsersFromTeamAsync(int teamIdP,
    int[] usersToRemoveP)
  {
    var input = new { user_ids = usersToRemoveP };
    return await PutAsync($"teams/{teamIdP}/remove_users", input);
  }

  public async Task<HttpResponseMessage> RemoveAllUsersFromTeamAsync(int teamIdP)
  {
    return await _nokoClient.PutAsync($"teams/{teamIdP}/remove_all_users", null);
  }

  public async Task<HttpResponseMessage> DeleteTeamAsync(int teamIdP)
  {
    return await DeleteAsync($"teams/{teamIdP}");
  }

  private async Task<HttpResponseMessage> GetResponseAsync(string requestUriP,
    IDictionary<string, string>? queryParamsP = null)
  {
    if (queryParamsP != null)
    {
      requestUriP = QueryHelpers.AddQueryString(requestUriP, queryParamsP);
    }

    var response = await _nokoClient.GetAsync(requestUriP);

    response.EnsureSuccessStatusCode();
    return response;
  }

  private async Task<HttpResponseMessage> PostAsync<T>(string requestUriP, T entityP)
  {
    var response = await _nokoClient.PostAsJsonAsync(requestUriP, entityP, _jsonOptions);
    response.EnsureSuccessStatusCode();
    return response;
  }

  private async Task<T> GetAsync<T>(string requestUriP, IDictionary<string, string>? queryParamsP = null)
  {
    var response = await GetResponseAsync(requestUriP, queryParamsP);
    var content = await response.Content.ReadAsStringAsync();
    return JsonSerializer.Deserialize<T>(content, _jsonOptions)!;
  }

  private async Task<HttpResponseMessage> DeleteAsync(string requestUriP)
  {
    var response = await _nokoClient.DeleteAsync(requestUriP);
    response.EnsureSuccessStatusCode();
    return response;
  }

  private async Task<HttpResponseMessage> PutAsync<T>(string requestUriP,
    T toBeChangedP)
  {
    var response = await _nokoClient.PutAsJsonAsync(requestUriP, toBeChangedP, _jsonOptions);
    return response;
  }

  private async Task<List<T>> GetAllAsync<T>(string requestUriP, IDictionary<string, string>? queryParamsP = null)
  {
    var link_header = new LinkHeader();
    var all_items = new List<T>();
    var uri = requestUriP;
    var has_page_links = true;
    do
    {
      var response = await GetResponseAsync(uri!, queryParamsP);
      try
      {
        response.Headers.GetValues("Link");
      }
      catch (InvalidOperationException)
      {
        has_page_links = false;
      }

      if (has_page_links)
      {
        link_header.GetLinksFromHeader(response.Headers.GetValues("Link").FirstOrDefault()!);
      }

      var content = await response.Content.ReadAsStringAsync();
      var items = JsonSerializer.Deserialize<List<T>>(content, _jsonOptions);

      all_items.AddRange(items!);
      if (link_header.Next == uri)
      {
        link_header.Next = null;
      }
      else
      {
        uri = link_header.Next;
      }
    } while (link_header.Next != null);

    return all_items;
  }
}