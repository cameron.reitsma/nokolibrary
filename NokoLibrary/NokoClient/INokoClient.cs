using NokoClient.Model;

namespace NokoClient;

public interface INokoClient
{
  public Task<List<NokoProject>> GetProjectsAsync(IDictionary<string, string>? queryParamsP = null);
  public Task<NokoProject> GetProjectAsync(int idP);
  public Task<HttpResponseMessage> CreateProjectAsync(NokoProjectCreator nokoProjectP);
  public Task<List<NokoEntry>> GetProjectEntriesAsync(int projectIdP, IDictionary<string, string>? queryParamsP = null);
  public Task<List<NokoExpense>> GetProjectExpensesAsync(int projectIdP, IDictionary<string, string>? queryParamsP = null);
  public Task<HttpResponseMessage> EditProjectAsync(int idP, NokoProjectEditor nokoProjectP);
  public Task<HttpResponseMessage> MergeProjectsAsync(int sourceProjectIdP, int projectToBeMergedIdP);
  public Task<HttpResponseMessage> DeleteProjectAsync(int projectIdP);
  public Task<HttpResponseMessage> ArchiveProjectAsync(int projectIdP);
  public Task<HttpResponseMessage> UnarchiveProjectAsync(int projectIdP);
  public Task<HttpResponseMessage> ArchiveMultipleProjectsAsync(int[] projectIdsP);
  public Task<HttpResponseMessage> UnarchiveMultipleProjectsAsync(int[] projectIdsP);
  public Task<HttpResponseMessage> DeleteMultipleProjectsAsync(int[] projectIdsP);
  public Task<List<NokoTag>> GetTagsAsync(IDictionary<string, string>? queryParamsP = null);
  public Task<HttpResponseMessage> CreateMultipleTagsAsync(string[] namesP);
  public Task<NokoTag> GetTagAsync(int tagIdP);
  public Task<List<NokoEntry>> GetTagEntriesAsync(int tagIdP, IDictionary<string, string>? queryParamsP = null);
  public Task<HttpResponseMessage> EditTagAsync(int idP, string nameP);
  public Task<HttpResponseMessage> MergeTagsAsync(int sourceTagIdP, int tagToBeMergedIdP);
  public Task<HttpResponseMessage> DeleteTagAsync(int idP);
  public Task<HttpResponseMessage> DeleteMultipleTagsAsync(int[] tagIdsP);
  public Task<List<NokoTimer>> GetTimersAsync(IDictionary<string, string>? queryParamsP = null);
  public Task<NokoTimer> GetProjectTimerAsync(int projectIdP);
  public Task<HttpResponseMessage> EditProjectTimerAsync(int projectIdP, string descriptionP);
  public Task<HttpResponseMessage> StartProjectTimerAsync(int projectIdP, DateOnly? dateP, string? descriptionP);
  public Task<HttpResponseMessage> PauseProjectTimerAsync(int projectIdP);
  public Task<HttpResponseMessage> AddOrSubtractFromTimerAsync(int projectIdP, int? minutesP, int? secondsP);
  public Task<HttpResponseMessage> LogProjectTimerAsync(int projectIdP, DateOnly? entryDateP, int? minutesP,
    string? descriptionP);
  public Task<HttpResponseMessage> DiscardProjectTimerAsync(int projectIdP);
  public Task<List<NokoProjectGroup>> GetProjectGroupsAsync(IDictionary<string, string>? queryParamsP = null);
  public Task<NokoProjectGroup> GetProjectGroupAsync(int projectGroupIdP);
  public Task<HttpResponseMessage> CreateProjectGroupAsync(NokoProjectGroupCreator nokoProjectGroupP);
  public Task<HttpResponseMessage> EditProjectGroupAsync(int projectGroupIdP, string nameP);
  public Task<List<NokoEntry>> GetProjectGroupProjectEntriesAsync(int projectGroupIdP,
    IDictionary<string, string>? queryParamsP = null);
  public Task<List<NokoProject>> GetProjectGroupProjectsAsync(int projectGroupIdP,
    IDictionary<string, string>? queryParamsP = null);
  public Task<HttpResponseMessage> AddProjectsToProjectGroupAsync(int projectGroupIdP, int[] projectIdsP);
  public Task<HttpResponseMessage> RemoveProjectsFromProjectGroupAsync(int projectGroupIdP,
    IDictionary<string, int[]> projectsToRemoveP);
  public Task<HttpResponseMessage> RemoveAllProjectsFromProjectGroupAsync(int projectGroupIdP);
  public Task<HttpResponseMessage> DeleteProjectGroupAsync(int projectGroupIdP);
  public Task<List<NokoEntry>> GetEntriesAsync(IDictionary<string, string>? queryParamsP = null);
  public Task<NokoEntry> GetEntryAsync(int entryIdP);
  public Task<HttpResponseMessage> CreateEntryAsync(NokoEntryCreator nokoEntryP);
  public Task<HttpResponseMessage> EditEntryAsync(int entryIdP, NokoEntryEditor nokoEntryP);
  public Task<HttpResponseMessage> MarkEntryAsInvoicedAsync(int entryIdP, DateOnly dateP);
  public Task<HttpResponseMessage> MarkMultipleEntriesAsInvoicedAsync(int[] entryIdsP, DateOnly dateP);
  public Task<HttpResponseMessage> MarkEntryApprovedAsync(int entryIdP, DateOnly? dateP = null);
  public Task<HttpResponseMessage> MarkMultipleEntriesAsApprovedAsync(int[] entryIdsP, DateOnly? approvedAtP = null);
  public Task<HttpResponseMessage> MarkEntryUnapprovedAsync(int entryIdP);
  public Task<HttpResponseMessage> MarkMultipleEntriesAsUnapprovedAsync(int[] entryIdsP);
  public Task<HttpResponseMessage> DeleteEntryAsync(int entryIdP);
  public Task<List<NokoInboxEntry>> GetInboxEntriesAsync(IDictionary<string, string>? queryParamsP = null);
  public Task<NokoInboxEntry> GetInboxEntryAsync(int entryIdP);
  public Task<HttpResponseMessage> CreateInboxEntryAsync(NokoInboxEntryCreator nokoInboxEntryP);
  public Task<HttpResponseMessage> EditInboxEntryAsync(int entryIdP, NokoInboxEntryEditor nokoInboxEntryP);
  public  Task<HttpResponseMessage> LogInboxEntryAsync(int entryIdP, NokoEntryCreator nokoEntryP);
  public Task<HttpResponseMessage> DeleteInboxEntryAsync(int entryIdP);
  public Task<List<NokoInvoice>> GetInvoicesAsync(IDictionary<string, string>? queryParamsP = null);
  public Task<NokoInvoice> GetInvoiceAsync(int invoiceIdP);
  public Task<HttpResponseMessage> CreateInvoiceAsync(NokoInvoiceCreator nokoInvoiceP);
  public Task<HttpResponseMessage> EditInvoiceAsync(int invoiceIdP, NokoInvoiceEditor nokoInvoiceP);
  public Task<HttpResponseMessage> MarkInvoicePayedAsync(int invoiceIdP);
  public Task<HttpResponseMessage> MarkInvoiceUnpaidAsync(int invoiceIdP);
  public Task<List<NokoEntry>> GetInvoiceEntriesAsync(int invoiceIdP, IDictionary<string, string>? queryParamsP = null);
  public Task<List<NokoExpense>> GetInvoiceExpensesAsync(int invoiceIdP,
    IDictionary<string, string>? queryParamsP = null);
  public Task<HttpResponseMessage> AddEntriesToInvoiceAsync(int invoiceIdP, int[] entryIdsP);
  public Task<HttpResponseMessage> RemoveEntriesFromInvoiceAsync(int invoiceIdP, int[] entryIdsP);
  public Task<HttpResponseMessage> RemoveAllEntriesFromInvoiceAsync(int invoiceIdP);
  public Task<HttpResponseMessage> AddExpensesToInvoiceAsync(int invoiceIdP, int[] expenseIdsP);
  public Task<HttpResponseMessage> RemoveExpensesFromInvoiceAsync(int invoiceIdP, int[] expenseIdsP);
  public Task<HttpResponseMessage> RemoveAllExpensesFromInvoiceAsync(int invoiceIdP);
  public Task<HttpResponseMessage> AddTaxesToInvoiceAsync(int invoiceIdP, NokoTaxCreator[] nokoTaxCreatorsP);
  public Task<HttpResponseMessage> RemoveTaxesFromInvoiceAsync(int invoiceIdP, int[] taxIdsP);
  public Task<HttpResponseMessage> RemoveAllTaxesFromInvoiceAsync(int invoiceIdP);
  public Task<HttpResponseMessage> DeleteInvoiceAsync(int invoiceIdP);
  public Task<List<NokoExpense>> GetExpensesAsync(IDictionary<string, string>? queryParamsP = null);
  public Task<NokoExpense> GetExpenseAsync(int expenseIdP);
  public Task<HttpResponseMessage> CreateExpenseAsync(NokoExpenseCreator nokoExpenseP);
  public Task<HttpResponseMessage> EditExpenseAsync(int expenseIdP, NokoExpenseEditor nokoExpenseP);
  public Task<HttpResponseMessage> DeleteExpenseAsync(int expenseIdP);
  public Task<NokoAccount> GetAccountAsync();
  public Task<NokoCurrentUser> GetCurrentUserAsync();
  public Task<List<NokoEntry>> GetCurrentUserEntriesAsync(IDictionary<string, string>? queryParamsP = null);
  public Task<List<NokoExpense>> GetCurrentUserExpensesAsync(IDictionary<string, string>? queryParamsP = null);
  public Task<HttpResponseMessage> EditCurrentUserAsync(NokoCurrentUserEditor currentUserP);
  public Task<List<NokoUser>> GetUsersAsync(IDictionary<string, string>? queryParamsP = null);
  public Task<NokoUser> GetUserAsync(int userIdP);
  public Task<List<NokoEntry>> GetUserEntriesAsync(int userIdP, IDictionary<string, string>? queryParamsP = null);
  public Task<List<NokoExpense>> GetUserExpensesAsync(int userIdP, IDictionary<string, string>? queryParamsP = null);
  public Task<HttpResponseMessage> CreateUserAsync(NokoUserCreator nokoUserP);
  public Task<HttpResponseMessage> EditUserAsync(int userIdP, NokoUserEditor nokoUserP);
  public Task<HttpResponseMessage> ReactivateUserAsync(int userIdP);
  public Task<HttpResponseMessage> GiveUserAccessToProjectsAsync(int userIdP, int[] projectIdsP);
  public Task<HttpResponseMessage> RevokeUserAccessToProjectsAsync(int userIdP, int[] projectIdsP);
  public Task<HttpResponseMessage> RevokeUserAccessToAllProjectsAsync(int userIdP);
  public Task<HttpResponseMessage> DeleteUserAsync(int userIdP);
  public Task<HttpResponseMessage> DeactivateUserAsync(int userIdP);
  public Task<List<NokoTeam>> GetTeamsAsync(IDictionary<string, string>? queryParamsP = null);
  public Task<NokoTeam> GetTeamAsync(int teamIdP);
  public Task<HttpResponseMessage> CreateTeamAsync(NokoTeamCreator nokoTeamP);
  public Task<HttpResponseMessage> EditTeamAsync(int teamIdP, string teamNameP);
  public Task<List<NokoEntry>> GetTeamUserEntriesAsync(int teamIdP, IDictionary<string, string>? queryParamsP = null);
  public Task<List<NokoUser>> GetTeamUsersAsync(int teamIdP, IDictionary<string, string>? queryParamsP = null);
  public Task<HttpResponseMessage> AddUsersToTeamAsync(int teamIdP, int[] userIdsP);
  public Task<HttpResponseMessage> RemoveUsersFromTeamAsync(int teamIdP,
    int[] usersToRemoveP);
  public Task<HttpResponseMessage> RemoveAllUsersFromTeamAsync(int teamIdP);
  public Task<HttpResponseMessage> DeleteTeamAsync(int teamIdP);
}