using System.Text.Json.Serialization;

namespace NokoClient.Model;

public record NokoRateCalculation
{
  [JsonPropertyName("calculation_method")]
  public string? CalculationMethod { get; set; }
  [JsonPropertyName("standard_hourly_rate")]
  public double? StandardHourlyRate { get; set; }
  [JsonPropertyName("custom_hourly_rates")]
  public NokoCustomHourlyRate[]? CustomHourlyRates { get; set; }
  [JsonPropertyName("flat_rate")]
  public double? FlatRate { get; set; }
}

/// <summary>
/// flat_rate
///<para>Required if payment_type equals flat_rate decimal 
///The flat rate used as the total amount for hours worked.
/// </para>
///standard_hourly_rate
///<para>Required if calculation_method equals standard_hourly_rate or custom_hourly_rate decimal 
///The standard hourly rate used to calculate the total amount for hours worked.
/// </para>
///custom_hourly_rates
///<para>Required if calculation_method equals custom_hourly_rate (ignored otherwise) array of objects 
///The custom hourly rates for users, which are used to calculate the total amount for the hours they worked in this invoice.
/// </para>
/// </summary>
public record NokoRateCalculationCreator
{
  [JsonPropertyName("calculation_method")]
  public required string CalculationMethod { get; set; }
  [JsonPropertyName("flat_rate")]
  public double? FlatRate { get; set; }
  [JsonPropertyName("standard_hourly_rate")]
  public double? StandardHourlyRate { get; set; }
  [JsonPropertyName("custom_hourly_rates")]
  public NokoCustomHourlyRate[]? CustomHourlyRate { get; set; }
}