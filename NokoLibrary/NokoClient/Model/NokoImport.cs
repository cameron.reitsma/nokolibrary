namespace NokoClient.Model;

public record NokoImport
{
  public int? Id { get; set; }
  public string? Url { get; set; }
}