using System.Text.Json.Serialization;

namespace NokoClient.Model;

public record NokoPayment
{
  [JsonPropertyName("paid_at")]
  public DateTime? PaidAt { get; set; }
  public string? Reference { get; set; }
  [JsonPropertyName("payment_method")]
  public string? PaymentMethod { get; set; }
}