using System.Text.Json.Serialization;

namespace NokoClient.Model;

public record NokoInvoice
{
    public int? Id { get; set; }
    public string? State { get; set; }
    public string? Reference { get; set; }
    [JsonPropertyName("invoiced_date")]
    public DateOnly InvoicedDate { get; set; }
    [JsonPropertyName("project_name")]
    public string? ProjectName { get; set; }
    [JsonPropertyName("company_name")]
    public string? CompanyName { get; set; }
    [JsonPropertyName("company_details")]
    public string? CompanyDetails { get; set; }
    [JsonPropertyName("recipient_details")]
    public string? RecipientDetails { get; set; }
    public string? Description { get; set; }
    public string? Footer { get; set; }
    [JsonPropertyName("show_hours_worked")]
    public bool? ShowHoursWorked { get; set; }
    [JsonPropertyName("show_full_report")]
    public bool? ShowFullReport { get; set; }
    [JsonPropertyName("show_user_summaries")]
    public bool? ShowUserSummaries { get; set; }
    [JsonPropertyName("show_project_summaries")]
    public bool? ShowProjectSummaries { get; set; }
    [JsonPropertyName("show_project_name_for_expenses")]
    public bool? ShowProjectNameForExpenses { get; set; }
    public NokoCustomization? Customization { get; set; }
    [JsonPropertyName("rate_calculation")]
    public NokoRateCalculation? RateCalculation { get; set; }
    public NokoTax[]? Taxes { get; set; }
    [JsonPropertyName("taxable_amount")]
    public double? TaxableAmount { get; set; }
    [JsonPropertyName("taxfree_amount")]
    public double? TaxfreeAmount { get; set; }
    [JsonPropertyName("tax_total_amount")]
    public double? TaxTotalAmount { get; set; }
    [JsonPropertyName("total_amount")]
    public double? TotalAmount { get; set; }
    [JsonPropertyName("taxable_amount_with_currency")]
    public string? TaxableAmountWithCurrency { get; set; }
    [JsonPropertyName("taxfree_amount_with_currency")]
    public string? TaxfreeAmountWithCurrency { get; set; }
    [JsonPropertyName("tax_total_amount_with_currency")]
    public string? TaxTotalAmountWithCurrency { get; set; }
    [JsonPropertyName("total_amount_with_currency")]
    public string? TotalAmountWithCurrency { get; set; }
    public NokoPayment? Payment { get; set; }
    [JsonPropertyName("payment_transactions")]
    public NokoPaymentTransaction[]? PaymentTransactions { get; set; }
    public NokoProject[]? Projects { get; set; }
    public int? Entries { get; set; }
    [JsonPropertyName("entries_url")]
    public string? EntriesUrl { get; set; }
    public int? Expenses { get; set; }
    [JsonPropertyName("expenses_url")]
    public string? ExpensesUrl { get; set; }
    [JsonPropertyName("share_url")]
    public string? ShareUrl { get; set; }
    [JsonPropertyName("created_at")]
    public DateTime? CreatedAt { get; set; }
    [JsonPropertyName("updated_at")]
    public DateTime? UpdatedAt { get; set; }
    public string? Url { get; set; }
}

public record NokoInvoiceCreator
{
    [JsonPropertyName("invoice_date")]
    public required DateOnly InvoiceDate { get; set; }
    [JsonPropertyName("reference")]
    public string? Reference { get; set; }
    [JsonPropertyName("project_name")]
    public string? ProjectName { get; set; }
    [JsonPropertyName("company_name")]
    public string? CompanyName { get; set; }
    [JsonPropertyName("company_details")]
    public string? CompanyDetails { get; set; }
    [JsonPropertyName("recipient_details")]
    public string? RecipientDetails { get; set; }
    [JsonPropertyName("description")]
    public string? Description { get; set; }
    [JsonPropertyName("footer")]
    public string? Footer { get; set; }
    [JsonPropertyName("show_hours_worked")]
    public bool? ShowHoursWorked { get; set; }
    [JsonPropertyName("show_full_report")]
    public bool? ShowFullReport { get; set; }
    [JsonPropertyName("show_user_summaries")]
    public bool? ShowUserSummaries { get; set; }
    [JsonPropertyName("show_project_summaries")]
    public bool? ShowProjectSummaries { get; set; }
    [JsonPropertyName("show_project_name_for_expenses")]
    public bool? ShowProjectNameForExpenses { get; set; }
    [JsonPropertyName("rate_calculation")]
    public NokoRateCalculation? RateCalculation { get; set; }
    [JsonPropertyName("entry_ids")]
    public int[]? EntryIds { get; set; }
    [JsonPropertyName("expense_ids")]
    public int[]? ExpenseIds { get; set; }
    [JsonPropertyName("taxes")]
    public NokoTaxCreator[]? Taxes { get; set; }
    [JsonPropertyName("customization")]
    public NokoCustomizationCreator? NokoCustomizationCreator { get; set; }
}
public record NokoInvoiceEditor
{
    [JsonPropertyName("invoice_date")]
    public DateOnly? InvoiceDate { get; set; }
    [JsonPropertyName("reference")]
    public string? Reference { get; set; }
    [JsonPropertyName("project_name")]
    public string? ProjectName { get; set; }
    [JsonPropertyName("company_name")]
    public string? CompanyName { get; set; }
    [JsonPropertyName("company_details")]
    public string? CompanyDetails { get; set; }
    [JsonPropertyName("recipient_details")]
    public string? RecipientDetails { get; set; }
    [JsonPropertyName("description")]
    public string? Description { get; set; }
    [JsonPropertyName("footer")]
    public string? Footer { get; set; }
    [JsonPropertyName("show_hours_worked")]
    public bool? ShowHoursWorked { get; set; }
    [JsonPropertyName("show_full_report")]
    public bool? ShowFullReport { get; set; }
    [JsonPropertyName("show_user_summaries")]
    public bool? ShowUserSummaries { get; set; }
    [JsonPropertyName("show_project_summaries")]
    public bool? ShowProjectSummaries { get; set; }
    [JsonPropertyName("show_project_name_for_expenses")]
    public bool? ShowProjectNameForExpenses { get; set; }
    [JsonPropertyName("rate_calculation")]
    public NokoRateCalculation? RateCalculation { get; set; }
    [JsonPropertyName("customization")]
    public NokoCustomizationCreator? NokoCustomizationCreator { get; set; }
}