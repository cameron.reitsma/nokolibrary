using System.Text.Json.Serialization;

namespace NokoClient.Model;

public record NokoApprovedBy
{
  public int? Id { get; set; }
  public string? Email { get; set; }
  [JsonPropertyName("first_name")]
  public string? FirstName { get; set; }
  [JsonPropertyName("last_name")]
  public string? LastName { get; set; }
  [JsonPropertyName("profile_image_url")]
  public string? ProfileImageUrl { get; set; }
  public string? Url { get; set; }
}