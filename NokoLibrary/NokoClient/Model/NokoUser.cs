using System.Text.Json.Serialization;

namespace NokoClient.Model;

public record NokoUser
{
  public int? Id { get; set; }
  public string? Email { get; set; }
  [JsonPropertyName("first_name")]
  public string? FirstName { get; set; }
  [JsonPropertyName("last_name")] 
  public string? LastName { get; set; }
  [JsonPropertyName("profile_image_url")]
  public string? ProfileImageUrl { get; set; }
  public string? Url { get; set; }
  public string? State { get; set; }
  public string? Role { get; set; }
  public NokoTeam[]? Teams { get; set; }
  public int? Entries { get; set; }
  [JsonPropertyName("entries_url")]
  public string? EntriesUrl { get; set; }
  public int? Expenses { get; set; }
  [JsonPropertyName("expenses_url")]
  public string? ExpensesUrl { get; set; }
  [JsonPropertyName("give_access_to_project_url")] 
  public string? GiveAccessToProjectUrl { get; set; }
  [JsonPropertyName("revoke_access_to_project_url")] 
  public string? RevokeAccessToProjectUrl { get; set; }
  [JsonPropertyName("revoke_access_to_all_projects_url")] 
  public string? RevokeAccessToAllProjectsUrl { get; set; }
  [JsonPropertyName("activate_url")] 
  public string? ActivateUrl { get; set; }
  [JsonPropertyName("deactivate_url")] 
  public string? DeactivateUrl { get; set; }
  [JsonPropertyName("created_at")]
  public DateTime? CreatedAt { get; set; }
  [JsonPropertyName("updated_at")] 
  public DateTime? UpdatedAt { get; set; }
}

/// <summary>
///The user’s role. Accepted values are: supervisor, leader (default), coworker, contractor
/// </summary>
public record NokoUserCreator
{
  [JsonPropertyName("email")]
  public required string Email { get; set; }
  [JsonPropertyName("first_name")]
  public string? FirstName { get; set; }
  [JsonPropertyName("last_name")]
  public string? LastName { get; set; }
  [JsonPropertyName("role")]
  public string? Role { get; set; }
}
/// <summary>
///The user’s role. Accepted values are: supervisor, leader, coworker, contractor
/// </summary>
public record NokoUserEditor
{
  [JsonPropertyName("first_name")]
  public string? FirstName { get; set; }
  [JsonPropertyName("last_name")]
  public string? LastName { get; set; }
  [JsonPropertyName("role")]
  public string? Role { get; set; }
}