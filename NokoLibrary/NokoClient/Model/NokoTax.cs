using System.Text.Json.Serialization;

namespace NokoClient.Model;

public record NokoTax
{
  public int? Id { get; set; }
  public string? Name { get; set; }
  public double? Percentage { get; set; }
}

public record NokoTaxCreator
{
  [JsonPropertyName("percentage")]
  public required double Percentage {get;set;}
  [JsonPropertyName("name")]
  public string? Name { get; set; }
}