using System.Text.Json.Serialization;

namespace NokoClient.Model;

public record NokoCurrentUser
{
  public int? Id { get; set; }
  public string? Email { get; set; }
  [JsonPropertyName("first_name")]
  public string? FirstName { get; set; }
  [JsonPropertyName("last_name")] 
  public string? LastName { get; set; }
  [JsonPropertyName("profile_image_url")]
  public string? ProfileImageUrl { get; set; }
  public string? Url { get; set; }
  public string? State { get; set; }
  public string? Role { get; set; }
  public int? Entries { get; set; }
  [JsonPropertyName("entries_url")]
  public string? EntriesUrl { get; set; }
  public int? Expenses { get; set; }
  [JsonPropertyName("expenses_url")]
  public string? ExpensesUrl { get; set; }
  [JsonPropertyName("time_format")]
  public string? TimeFormat { get; set; }
  [JsonPropertyName("week_start")]
  public string? WeekStart { get; set; }
  [JsonPropertyName("utc_offset")]
  public double? UtcOffset { get; set; }
  [JsonPropertyName("send_personal_weekly_report_email")]
  public bool? SendPersonalWeeklyReportEmail { get; set; }
  [JsonPropertyName("send_team_weekly_report_email")]
  public bool? SendTeamWeeklyReportEmail { get; set; }
  [JsonPropertyName("created_at")]
  public DateTime? CreatedAt { get; set; }
  [JsonPropertyName("updated_at")]
  public DateTime? UpdatedAt { get; set; }
}

/// <summary>
///<para>
///Time Format Accepted Values: fraction, hours_minutes
/// </para>
/// <para>
///Week Start Accepted Values: sunday, monday
/// </para>
/// </summary>
public record NokoCurrentUserEditor
{
  [JsonPropertyName("first_name")]
  public string? FirstName { get; set; }
  [JsonPropertyName("last_name")]
  public string? LastName { get; set; }
  [JsonPropertyName("time_format")]
  public string? TimeFormat { get; set; }
  [JsonPropertyName("week_start")]
  public string? WeekStart { get; set; }
  [JsonPropertyName("send_personal_weekly_report")]
  public bool? SendPersonalWeeklyReport { get; set; }
  [JsonPropertyName("send_team_weekly_report")]
  public bool? SendTeamWeeklyReport { get; set; }
}