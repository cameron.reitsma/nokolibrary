using System.Text.Json.Serialization;

namespace NokoClient.Model;

public record NokoCustomization
{
    public string? Title { get; set; }
    public string? Date { get; set; }
    public string? Project { get; set; }
    public string? Reference { get; set; }
    [JsonPropertyName("total_due")]
    public string? TotalDue { get; set; }
    public string? Summary { get; set; }
    [JsonPropertyName("project_summary")]
    public string? ProjectSummary { get; set; }
    [JsonPropertyName("user_summary")]
    public string? UserSummary { get; set; }
    [JsonPropertyName("no_project_name")]
    public string? NoProjectName { get; set; }
    [JsonPropertyName("work_time")]
    public string? WorkTime { get; set; }
    [JsonPropertyName("no_tax")]
    public string? NoTax { get; set; }
    [JsonPropertyName("work_total")]
    public string? WorkTotal { get; set; }
    public string? Total { get; set; }
    public string? Report { get; set; }
    public string? Locale { get; set; }
    [JsonPropertyName("currency_code")]
    public string? CurrencyCode { get; set; }
    [JsonPropertyName("currency_symbol")]
    public string? CurrencySymbol { get; set; }
    [JsonPropertyName("taxable_total")]
    public string? TaxableTotal { get; set; }
    [JsonPropertyName("tax_total")]
    public string? TaxTotal { get; set; }
    [JsonPropertyName("taxfree_total")]
    public string? TaxfreeTotal { get; set; }
    [JsonPropertyName("total_report")]
    public string? TotalReport { get; set; }
    [JsonPropertyName("custom_html")]
    public string? CustomHtml { get; set; }
    [JsonPropertyName("allow_paypal_invoice")]
    public bool? AllowPaypalInvoice { get; set; }
    [JsonPropertyName("paypal_invoice_title")]
    public string? PaypalInvoiceTitle { get; set; }
    [JsonPropertyName("paypal_currency_code")]
    public string? PaypalCurrencyCode { get; set; }
    [JsonPropertyName("paypal_address")]
    public string? PaypalAddress { get; set; }
    [JsonPropertyName("allow_stripe_connect_payment")]
    public bool? AllowStripeConnectPayment { get; set; }
    [JsonPropertyName("stripe_connect_payment_description")]
    public string? StripeConnectPaymentDescription { get; set; }
    [JsonPropertyName("stripe_connect_currency_code")]
    public string? StripeConnectCurrencyCode { get; set; }
    [JsonPropertyName("currency_name")]
    public string? CurrencyName { get; set; }
    [JsonPropertyName("created_at")]
    public DateTime? CreatedAt { get; set; }
    [JsonPropertyName("updated_at")]
    public DateTime? UpdatedAt { get; set; }
}

public record NokoCustomizationCreator
{
    [JsonPropertyName("title")]
    public string? Title { get; set; }
    [JsonPropertyName("date")]
    public string? Date { get; set; }
    [JsonPropertyName("project")]
    public string? Project { get; set; }
    [JsonPropertyName("reference")]
    public string? Reference { get; set; }
    [JsonPropertyName("total_due")]
    public string? TotalDue { get; set; }
    [JsonPropertyName("summary")]
    public string? Summary { get; set; }
    [JsonPropertyName("project_summary")]
    public string? ProjectSummary { get; set; }
    [JsonPropertyName("user_summary")]
    public string? UserSummary { get; set; }
    [JsonPropertyName("no_project_name")]
    public string? NoProjectName { get; set; }
    [JsonPropertyName("work_time")]
    public string? WorkTime { get; set; }
    [JsonPropertyName("no_tax")]
    public string? NoTax { get; set; }
    [JsonPropertyName("work_total")]
    public string? WorkTotal { get; set; }
    [JsonPropertyName("total")]
    public string? Total { get; set; }
    [JsonPropertyName("report")]
    public string? Report { get; set; }
    [JsonPropertyName("locale")]
    public string? Locale { get; set; }
    [JsonPropertyName("currency_code")]
    public string? CurrencyCode { get; set; }
    [JsonPropertyName("currency_symbol")]
    public string? CurrencySymbol { get; set; }
    [JsonPropertyName("taxable_total")]
    public string? TaxableTotal { get; set; }
    [JsonPropertyName("tax_total")]
    public string? TaxTotal { get; set; }
    [JsonPropertyName("taxfree_total")]
    public string? TaxfreeTotal { get; set; }
    [JsonPropertyName("total_report")]
    public string? TotalReport { get; set; }
    [JsonPropertyName("custom_html")]
    public string? CustomHtml { get; set; }
    [JsonPropertyName("paypal_invoice")]
    public PaypalInvoice? PaypalInvoice { get; set; }
    [JsonPropertyName("striped_connect_payment")]
    public StripeConnectPayment? StripeConnectPayment { get; set; }
}

public record PaypalInvoice
{
    [JsonPropertyName("paypal_address")]
    public string? PaypalAddress { get; set; }
    [JsonPropertyName("paypal_invoice_title")]
    public string? PaypalInvoiceTitle { get; set; }
    [JsonPropertyName("paypal_currency_code")]
    public string? PaypalCurrencyCode { get; set; }
}

public record StripeConnectPayment
{
    [JsonPropertyName("payment_description")]
    public string? PaymentDescription { get; set; }
    [JsonPropertyName("currency_code")]
    public string? CurrencyCode { get; set; }
}