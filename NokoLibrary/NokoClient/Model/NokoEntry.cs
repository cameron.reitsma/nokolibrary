using System.Text.Json.Serialization;

namespace NokoClient.Model;

public class NokoEntry
{
  public int? Id { get; set; }
  public DateOnly? Date { get; set; }
  public NokoUser? User { get; set; }
  public bool? Billable { get; set; }
  public int? Minutes { get; set; }
  public string? Description { get; set; }
  public NokoProject? Project { get; set; }
  public NokoTag[]? Tags { get; set; }
  [JsonPropertyName("source_url")] 
  public string? SourceUrl { get; set; }
  [JsonPropertyName("invoiced_at")]
  public DateTime? InvoicedAt { get; set; }
  public NokoInvoice? Invoice { get; set; }
  public NokoImport? Import { get; set; }
  [JsonPropertyName("approved_at")] 
  public DateTime? ApprovedAt { get; set; }
  [JsonPropertyName("approved_by")]
  public NokoApprovedBy? ApprovedBy { get; set; }
  public string? Url { get; set; }
  [JsonPropertyName("invoiced_outside_of_noko_url")]
  public string? InvoicedOutsideOfNokoUrl { get; set; }
  [JsonPropertyName("approved_url")]
  public string? ApprovedUrl { get; set; }
  [JsonPropertyName("unapproved_url")] 
  public string? UnapprovedUrl { get; set; }
  [JsonPropertyName("created_at")]
  public DateTime? CreatedAt { get; set; }
  [JsonPropertyName("updated_at")]
  public DateTime? UpdatedAt { get; set; }
}

public record NokoEntryCreator
{
  [JsonPropertyName("date")]
  public required DateOnly Date { get; set; }
  [JsonPropertyName("user_id")]
  public string? UserId { get; set; }
  [JsonPropertyName("minutes")]
  public required int Minutes { get; set; }
  [JsonPropertyName("description")] 
  public string? Description { get; set; }
  [JsonPropertyName("project_id")] 
  public int? ProjectId { get; set; }
  [JsonPropertyName("project_name")] 
  public string? ProjectName { get; set; }
  [JsonPropertyName("source_url")] 
  public string? SourceUrl { get; set; }
}

public record NokoEntryEditor
{
  [JsonPropertyName("date")]
  public DateOnly? Date { get; set; }
  [JsonPropertyName("user_id")]
  public string? UserId { get; set; }
  [JsonPropertyName("minutes")]
  public int? Minutes { get; set; }
  [JsonPropertyName("description")] 
  public string? Description { get; set; }
  [JsonPropertyName("project_id")] 
  public int? ProjectId { get; set; }
  [JsonPropertyName("project_name")] 
  public string? ProjectName { get; set; }
  [JsonPropertyName("source_url")] 
  public string? SourceUrl { get; set; }
}