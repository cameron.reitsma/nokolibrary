using System.Text.Json.Serialization;

namespace NokoClient.Model;

public record NokoCustomHourlyRate
{
  public required int Id { get; set; }
  [JsonPropertyName("hourly_rate")]
  public required double HourlyRate { get; set; }
}