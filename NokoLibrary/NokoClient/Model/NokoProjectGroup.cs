using System.Text.Json.Serialization;

namespace NokoClient.Model;

public record NokoProjectGroup
{
  public int? Id { get; set; }
  public string? Name { get; set; }
  public NokoProject[]? Projects { get; set; }
  public string? Url { get; set; }
  [JsonPropertyName("entries_url")]
  public string? EntriesUrl { get; set; }
  [JsonPropertyName("invoices_url")]
  public string? InvoicesUrl { get; set; }
  [JsonPropertyName("projects_url")]
  public string? ProjectsUrl { get; set; }
  [JsonPropertyName("created_at")]
  public DateTime CreatedAt { get; set; }
  [JsonPropertyName("updated_at")]
  public DateTime UpdatedAt { get; set; }
  [JsonPropertyName("add_projects_url")]
  public string? AddProjectsUrl { get; set; }
  [JsonPropertyName("remove_projects_url")]
  public string? RemoveProjectsUrl { get; set; }
  [JsonPropertyName("remove_all_projects_url")]
  public string? RemoveAllProjectsUrl { get; set; }
}

public record NokoProjectGroupCreator
{
  [JsonPropertyName("name")]
  public required string Name { get; set; }
  [JsonPropertyName("project_ids")]
  public required int[] Projects { get; set; }
}