using System.Text.Json.Serialization;

namespace NokoClient.Model;

public record NokoAccount
{
  public int? Id { get; set; }
  public string? Name { get; set; }
  [JsonPropertyName("invoicing_enabled")]
  public bool? InvoicingEnabled { get; set; }
  public NokoUser? Owner { get; set; }
  public string? Url { get; set; }
  [JsonPropertyName("created_at")]
  public DateTime? CreatedAt { get; set; }
  [JsonPropertyName("updated_at")]
  public DateTime? UpdatedAt { get; set; }
}