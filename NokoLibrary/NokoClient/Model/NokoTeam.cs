using System.Text.Json.Serialization;

namespace NokoClient.Model;

public record NokoTeam
{
  public int? Id { get; set; }
  public string? Name { get; set; }
  public NokoUser[]? Users { get; set; }
  private string? Url { get; set; }
  [JsonPropertyName("entries_url")]
  public string? EntriesUrl { get; set; }
  [JsonPropertyName("users_url")]
  public string? UsersUrl { get; set; }
  [JsonPropertyName("created_at")]
  public DateTime? CreatedAt { get; set; }
  [JsonPropertyName("updated_at")]
  public DateTime? UpdatedAt { get; set; }
  [JsonPropertyName("add_users_url")]
  public string? AddUsersUrl { get; set; }
  [JsonPropertyName("remove_users_url")]
  public string? RemoveUsersUrl { get; set; }
  [JsonPropertyName("remove_all_users_url")]
  public string? RemoveAllUsersUrl { get; set; }
}

public record NokoTeamCreator
{
  [JsonPropertyName("name")]
  public required string Name { get; set; }
  [JsonPropertyName("user_ids")]
  public required int[] UserIds { get; set; }
}