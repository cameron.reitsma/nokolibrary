﻿using System.Text.Json.Serialization;

namespace NokoClient.Model;

public record NokoProject
{
  public int? Id { get; set; }
  public string? Name { get; set; }
  [JsonPropertyName("billing_increment")]
  public int? BillingIncrement { get; set; }
  public bool? Enabled { get; set; }
  public bool? Billable { get; set; }
  public string? Color { get; set; }
  public string? Url { get; set; }
  public NokoGroup? Group { get; set; }
  public int? Minutes { get; set; }
  [JsonPropertyName("billable_minutes")]
  public int? BillableMinutes { get; set; }
  [JsonPropertyName("unbillable_minutes")]
  public int? UnbillableMinutes { get; set; }
  [JsonPropertyName("invoiced_minutes")]
  public int? InvoicedMinutes { get; set; }
  [JsonPropertyName("remaining_minutes")]
  public int? RemainingMinutes { get; set; }
  [JsonPropertyName("budgeted_minutes")]
  public int? BudgetedMinutes { get; set; }
  public NokoImport? Import { get; set; }
  public NokoInvoice[]? Invoices { get; set; }
  public NokoUser[]? Participants { get; set; }
  public int? Entries { get; set; }
  [JsonPropertyName("entries_url")]
  public string? EntriesUrl { get; set; }
  public double? Expenses { get; set; }
  [JsonPropertyName("expenses_url")]
  public string? ExpensesUrl { get; set; }
  [JsonPropertyName("created_at")]
  public DateTime? CreatedAt { get; set; }
  [JsonPropertyName("updated_at")]
  public DateTime? UpdatedAt { get; set; }
  [JsonPropertyName("merge_url")]
  public string? MergeUrl { get; set; }
  [JsonPropertyName("archive_url")]
  public string? ArchiveUrl { get; set; }
  [JsonPropertyName("unarchive_url")]
  public string? UnarchiveUrl { get; set; }
}

public record NokoProjectCreator
{
  [JsonPropertyName("name")] 
  public required string Name { get; set; }
  [JsonPropertyName("billable")]
  public bool? Billable { get; set; }
  [JsonPropertyName("project_group_id")]
  public int? ProjectGroupId { get; set; }
  [JsonPropertyName("billing_increment")]
  public int? BillingIncrement { get; set; }
  [JsonPropertyName("color")]
  public string? Color { get; set; }
}

public record NokoProjectEditor
{
  [JsonPropertyName("name")] 
  public string? Name { get; set; }
  [JsonPropertyName("project_group_id")]
  public int? ProjectGroupId { get; set; }
  [JsonPropertyName("billing_increment")]
  public int? BillingIncrement { get; set; }
  [JsonPropertyName("color")]
  public string? Color { get; set; }
}