﻿using System.Text.Json.Serialization;

namespace NokoClient.Model;

public record NokoTag
{
  public int? Id { get; set; }
  public string? Name { get; set; }
  public bool? Billable { get; set; }
  [JsonPropertyName("formatted_name")]
  public string? FormattedName { get; set; }
  public string? Url { get; set; }
  public NokoImport? Import { get; set; }
  public int? Entries { get; set; }
  [JsonPropertyName("entries_url")]
  public string? EntriesUrl { get; set; }
  [JsonPropertyName("merge_url")]
  public string? MergeUrl { get; set; }
  [JsonPropertyName("created_at")]
  public DateTime? CreatedAt { get; set; }
  [JsonPropertyName("updated_at")]
  public DateTime? UpdatedAt { get; set; }
}

public record NokoTagCreator
{
  [JsonPropertyName("names")]
  public required string[] Names { get; set; }
}