using System.Text.Json.Serialization;

namespace NokoClient.Model;

public record NokoPaymentTransaction
{
  [JsonPropertyName("created_at")]
  public DateTime? CreatedAt { get; set; }
  public string? Description { get; set; }
  [JsonPropertyName("payment_method")]
  public string? PaymentMethod { get; set; }
  public string? Reference { get; set; }
  [JsonPropertyName("invoice_state")]
  public string? InvoiceState { get; set; }
}