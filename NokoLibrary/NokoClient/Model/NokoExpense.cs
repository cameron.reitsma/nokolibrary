using System.Text.Json.Serialization;

namespace NokoClient.Model;

public record NokoExpense
{
  public int? Id { get; set; }
  public double? Price { get; set; }
  public string? Description { get; set; }
  public bool? Taxable { get; set; }
  public DateOnly? Date { get; set; }
  public NokoProject? Project { get; set; }
  public NokoInvoice? Invoice { get; set; }
  public NokoUser? User { get; set; }
  public string? Url { get; set; }
  [JsonPropertyName("created_at")]
  public DateTime? CreatedAt { get; set; }
  [JsonPropertyName("updated_at")]
  public DateTime? UpdatedAt { get; set; }
}

public record NokoExpenseCreator
{
  [JsonPropertyName("date")]
  public required DateOnly Date { get; set; }
  [JsonPropertyName("project_id")]
  public required int ProjectId { get; set; }
  [JsonPropertyName("price")]
  public required decimal Price { get; set; }
  [JsonPropertyName("user_id")]
  public int? UserId { get; set; }
  [JsonPropertyName("taxable")]
  public bool? Taxable { get; set; }
  [JsonPropertyName("description")]
  public string? Description { get; set; }
}

public record NokoExpenseEditor
{
  [JsonPropertyName("date")]
  public DateOnly? Date { get; set; }
  [JsonPropertyName("project_id")]
  public int? ProjectId { get; set; }
  [JsonPropertyName("price")]
  public decimal? Price { get; set; }
  [JsonPropertyName("user_id")]
  public int? UserId { get; set; }
  [JsonPropertyName("taxable")]
  public bool? Taxable { get; set; }
  [JsonPropertyName("description")]
  public string? Description { get; set; }
}