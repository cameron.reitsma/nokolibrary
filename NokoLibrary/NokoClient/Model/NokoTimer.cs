﻿using System.Text.Json.Serialization;

namespace NokoClient.Model;

public record NokoTimer
{
  public int? Id { get; set; }
  public string? State { get; set; }
  public int? Seconds { get; set; }
  [JsonPropertyName("formatted_time")]
  public string? FormattedTime { get; set; }
  public DateOnly? Date { get; set; }
  public string? Description { get; set; }
  public NokoUser? User { get; set; }
  public NokoProject? Project { get; set; }
  public string? Url { get; set; }
  [JsonPropertyName("start_url")]
  public string? StartUrl { get; set; }
  [JsonPropertyName("pause_url")]
  public string? PauseUrl { get; set; }
  [JsonPropertyName("add_or_subtract_time_url")]
  public string? AddOrSubtractTimeUrl { get; set; }
  [JsonPropertyName("log_url")]
  public string? LogUrl { get; set; }
}