using System.Text.Json.Serialization;

namespace NokoClient.Model;

public record NokoInboxEntry
{
  public int? Id { get; set; }
  public DateOnly? Date { get; set; }
  public int? Minutes { get; set; }
  public string? Description { get; set; }
  [JsonPropertyName("project_name")]
  public string? ProjectName { get; set; }
  public bool? Logged { get; set; }
  [JsonPropertyName("entry_url")]
  public string? EntryUrl { get; set; }
  public string? Url { get; set; }
  [JsonPropertyName("log_url")]
  public string? LogUrl { get; set; }
  [JsonPropertyName("created_at")]
  public DateTime? CreatedAt { get; set; }
  [JsonPropertyName("updated_at")]
  public DateTime? UpdatedAt { get; set; }
}

public record NokoInboxEntryCreator
{
  [JsonPropertyName("date")]
  public required DateOnly Date { get; set; }
  [JsonPropertyName("description")]
  public required string Description { get; set; }
  [JsonPropertyName("minutes")]
  public int? Minutes { get; set; }
  [JsonPropertyName("project_name")]
  public string? ProjectName { get; set; } 
}

public record NokoInboxEntryEditor
{
  [JsonPropertyName("date")]
  public required DateOnly Date { get; set; }
  [JsonPropertyName("description")]
  public required string Description { get; set; }
  [JsonPropertyName("minutes")]
  public int Minutes { get; set; }
  [JsonPropertyName("project_name")]
  public string? ProjectName { get; set; } 
}